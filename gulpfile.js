const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    multipipe = require('multipipe'),
    sass = require('gulp-sass'),
    minCss = require('gulp-csso'),
    gDebug = require('gulp-debug'),
    gSMaps = require('gulp-sourcemaps'),
    gIf = require('gulp-if'),
    path = require('path'),
    pug = require('gulp-pug'),
    cached = require('gulp-cached'),
    remember = require('gulp-remember'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    webpack = require('webpack'),
    webpackStream = require('webpack-stream');
browserSync = require('browser-sync').create();

const paths = {
    assets: {
        pug: 'assets/pug/*.pug',
        js: 'assets/js/*.js',
        scss: 'assets/scss/*.scss',
        fonts: 'assets/fonts/*.ttf',
        data: 'assets/data/*',
    },
    watch: {
        pug: 'assets/pug/**/*.pug',
        js: 'assets/js/**/*.js',
        scss: 'assets/scss/**/*.scss',
        fonts: 'assets/fonts/**/*.ttf',
        data: 'assets/data/**/*',
    },
    public: {
        html: 'public',
        js: 'public/js',
        css: 'public/css',
        fonts: 'public/fonts',
        data: 'public/data',
    },
    dist: {
        html: 'dist',
        js: 'dist/js',
        css: 'dist/css',
        fonts: 'dist/fonts',
        data: 'dist/data',
    }
};

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

gulp.task('server', function () {
    browserSync.init({
        server: './public',
        files: 'index.html'
    });
    browserSync.watch(paths.public.html + '/**/*').on('change', browserSync.reload);
});

gulp.task('style:build', function () {
    return multipipe(
        gulp.src(paths.assets.scss),
        gIf(isDevelopment, cached('style')),
        gIf(isDevelopment, gSMaps.init()),
        gDebug({title: 'assets'}),
        sass(),
        minCss({
            restructure: false,
            debug: false
        }),
        autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }),
        remember('style:build'),
        gIf(isDevelopment, gSMaps.write('/maps')),
        gIf(isDevelopment, gulp.dest(paths.public.css),
            gulp.dest(paths.dist.css)),
        browserSync.reload({stream: true})
    ).on('error', notify.onError(function (err) {
            return {
                title: 'style:build',
                message: err.message
            };
        })
    );
});

gulp.task('pug:build', function () {
    return multipipe(
        gulp.src(paths.assets.pug),
        pug({
            pretty: true
        }),
        gIf(isDevelopment, gulp.dest(paths.public.html),
            gulp.dest(paths.dist.html))
    ).on('error', notify.onError(function (err) {
            return {
                title: 'pug:build',
                message: err.message
            };
        })
    );
});

gulp.task('scripts:build', function () {
    return multipipe(
        gulp.src(paths.assets.js),
        gIf(isDevelopment, cached('scripts')),
        gIf(isDevelopment, gSMaps.init()),
        webpackStream(webpack(require('./webpack.config.js'))),
        gIf(isDevelopment, gulp.dest(paths.public.js),
            gulp.dest(paths.dist.js)),
        uglify(),
        rename({suffix: '.min'}),
        gIf(isDevelopment, gSMaps.write('.')),
        gIf(isDevelopment, gulp.dest(paths.public.js),
            gulp.dest(paths.dist.js))
    ).on('error', notify.onError(function (err) {
            return {
                title: 'scripts:build',
                message: err.message
            };
        })
    );
});

gulp.task('watch', function () {
    gulp.watch(paths.watch.scss, gulp.series('style:build')).on('unlink', function (filepath) {
        remember.forget('style', path.resolve(filepath));
        delete cached.caches.style[path.resolve(filepath)];
    });
    gulp.watch(paths.watch.js, gulp.series('scripts:build')).on('unlink', function (filepath) {
        remember.forget('scripts', path.resolve(filepath));
        delete cached.caches.scripts[path.resolve(filepath)];
    });
    gulp.watch(paths.watch.pug, gulp.series('pug:build'))
});

gulp.task('dev', gulp.series('style:build', 'pug:build', /*'scripts:build',*/ 'watch'));
gulp.task('prod', gulp.series('style:build', 'pug:build', /*'scripts:build'*/));